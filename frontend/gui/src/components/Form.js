import React from 'react';
import { Form, Input, Button } from 'antd';
import axios from 'axios';

class CustomForm extends React.Component {
  constructor(props) {
     super(props);
     this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }  
    handleFormSubmit = (event, requestType, articleID) => {
        const title = event.target.elements.title.value;
        const content = event.target.elements.content.value
        console.log(title, content)
        switch (requestType) {
            case 'post':
               return axios.post('http://192.168.1.179/api/', {
                   title: title,
                   content: content
               })
               .then(res => console.log(res))
               .catch(err => console.log(err));
            case 'put':
                return axios.put(`http://192.168.1.179/api/${articleID}/`, {
                    title: title,
                    content: content
                })
               .then(res => console.log(res))
               .catch(err => console.log(err));
            default:
                break;
        
        }
        window.location.reload();
    }
    render() {
  return (
      <div>
      <Form onSubmitCapture={(event) => this.handleFormSubmit(event,
        this.props.requestType,
        this.props.articleID
        )}>
        <Form.Item label="Tile" name="title">
          <Input name='title' placeholder="Put a title here" />
        </Form.Item>
        <Form.Item label="Content" name="content">
          <Input name='content' placeholder="Enter some content" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">{this.props.btnText}</Button>
        </Form.Item>
      </Form>
      </div>
    )
    };
}

export default CustomForm;