import React from 'react';
import axios from 'axios';
import Articles from '../components/Article';
import CustomForm from '../components/Form';

class ArticleList extends React.Component {

    state = {
        articles: []
    }

    componentDidMount() {
        axios.get('http://192.168.1.179/api/')
            .then(res => {
                this.setState({
                    articles:res.data
                })
            })
    }
    
    render () {
        return (
            <div>
                <Articles data={this.state.articles}/>
                <CustomForm requestType='post' articleID={null} btnText='Submit' />
            </div>
        )
    }
}

export default ArticleList;